﻿using System;

namespace FatBitTimer
{
    public class FatTime
    {
        public DateTime Time { get; set; }
        public string Hour { get; set; }
        public int Minutes { get; set; }
    }
}
