﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FatBitTimer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("___________________");
            Console.WriteLine("    FatBitTimer    ");
            Console.WriteLine("___________________\n");

        run_again:

            var timeStart = GetInputTime("Horário inicial (HH:mm) > ");
            var timeEnd = GetInputTime("Horário final (HH:mm)   > ");
            var timeNow = GetInputTime("Horário atual (HH:mm)   > ");

            Console.WriteLine();
            Console.WriteLine($"Hora/minutos: Inicial {timeStart.Hour}/{timeStart.Minutes} | Final {timeEnd.Hour}/{timeEnd.Minutes} | Atual {timeNow.Hour}/{timeNow.Minutes}");
            Console.WriteLine();

            bool result = CheckTimeIsBetweenTimes(timeStart, timeEnd, timeNow);

            Console.WriteLine($"> A hora atual está dentro da hora inicial e final = {result}");

            Console.WriteLine($"\nPressione ENTER para rodar novamente...");

            var key = Console.ReadKey();

            if (key.Key == ConsoleKey.Enter)
                goto run_again;
        }

        private static FatTime GetInputTime(string message)
        {
            Console.Write(message);

        do_again:

            string time = Console.ReadLine();
            DateTime dateTime;

            if (time.Length != 5 || !time.Contains(":") || !DateTime.TryParseExact(time, "HH:mm", CultureInfo.InvariantCulture, DateTimeStyles.None, out dateTime))
            {
                Console.Write("Formato incorreto: ");
                goto do_again;
            }

            return new FatTime()
            {
                Time = dateTime,
                Hour = time,
                Minutes = GetTotalMinutesFromTime(time)
            };
        }

        private static int GetTotalMinutesFromTime(string time)
        {
            int separator = time.IndexOf(':');
            int hour = int.Parse(time.Substring(0, separator));
            int minute = int.Parse(time.Substring(separator + 1));

            return (hour * 60) + minute;
        }

        private static bool CheckTimeIsBetweenTimes(FatTime minStart, FatTime minEnd, FatTime minNow)
        {
            if (minStart.Minutes > minEnd.Minutes)
            {
                minEnd.Minutes += 24 * 60;

                if (minStart.Minutes > minNow.Minutes)
                    minNow.Minutes += 24 * 60;

                Console.WriteLine($"I: {minStart.Minutes} F: {minEnd.Minutes} A: {minNow.Minutes}");

                return (minStart.Minutes < minNow.Minutes && minNow.Minutes < minEnd.Minutes);
            }

            return (minStart.Minutes < minNow.Minutes && minNow.Minutes < minEnd.Minutes);
        }
    }
}
